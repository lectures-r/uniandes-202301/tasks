#==============================================================================#
# Autor: Eduard-Martinez
# Update: 
# R version 4.1.1 (2021-08-10)
#==============================================================================#

# clean environment and load packages
rm(list=ls())
require(pacman)
p_load(tidyverse,rio,janitor)

# Para este task debe:

## descargue los datos del siguiente link:
browseURL("https://microdatos.dane.gov.co/index.php/catalog/701/download/20156")

## 1. Import:
### Importe los datos de "Cabecera - Características generales" y "Cabecera - Ocupados" y guardelos en una carpeta llamada input.
### selecione las variables c(directorio, secuencia_p, orden, dpto, p6040, p6020, esc, p6220) de 
### caracteristicas y c(directorio, secuencia_p, orden, inglabo, p6426) de ocuados

cab = import("Input/Cabecera - Características generales (Personas).dta") %>%
        clean_names()
ocu = import("Input/Cabecera - Ocupados.dta") %>% 
        clean_names()
#--------#
cab = cab %>%
       select(directorio,secuencia_p, orden, dpto, p6040, p6020, esc, p6220) 

ocu = ocu %>%  
        select(directorio, secuencia_p, orden, inglabo, p6426)

## 2.Join.
### Realice un join  entre las dos bases de datos utilizando las variables c(directorio, secuencia_p, orden)
### asegúrese de quedarse solo con las personas que son ocupadas
output = left_join(ocu, data, by = c("directorio","secuencia_p", "orden"))

## 3. Descriptivas
### encuentre el ingreso promedio, experiencia promedio, mediana de la escolaridad, y la cantidad de personas, agrupando por
### las variables de c(dpto, p6020, p6220)
output = output %>% 
  group_by(dpto, p6020, p6220) %>% 
  summarise(mean_inglabo = mean(inglabo, na.rm = T),
            medin_esc = median(esc, na.rm = T),
            mean_exp = mean(p6426, na.rm = T),
            people = n())

## 4. Pivote en formato long todas las variables que se crearon en la anterior sección 
output = output %>% 
          pivot_longer(cols = 4:7, 
                       names_to = "variable", 
                       values_to = "value")


# Mini-diccionario  
#p6040 = años
#p6020 = sexo 
#Esc = años de escolaridad
#P6220 = Titulo de mayor nivel educativo
#INGLABO = Salario
#P6426 = Tiempo de trabajo en empresa